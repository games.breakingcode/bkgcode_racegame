v1.1
- Bug Fixes
- Shortcut changes
- Added auto-focusing on search field

v1.1.1
- Bug Fixes

v1.1.2
- Added non-dockable windows (closes automatically

v1.1.3
- Bug fixes and warnings
- Fixed Compatibility with Unity 2018

v1.1.4
- "Enter" shortcut for the first item in a search