Folder contains 3 packages of the "Vertex Ambient Occlusion Generator" asset designed for 3 different Unity render pipelines.

Based on a project’s render pipeline import only one of the included packages.


-------------------------------------------------------------------
Asset forum:            https://forum.unity.com/threads/vertex-ambient-occlusion-generator.356751/
Support and bug report: support@amazingassets.world