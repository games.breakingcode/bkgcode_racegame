﻿using UnityEngine;
using UnityEditor;

namespace Sauce3D
{
    public class BreakoutHelper : EditorWindow
    {
        #region Variables
        //Snap to Grid variables.
        private float gridSizeHorizontal = 0.25f;
        private float gridSizeVertical = 0.25f;
        public bool snapEnabled = false;
        private Vector3 posBefore = Vector3.zero;

        public bool infoSnappingEnabled = false;
        public bool infoPrefabsEnabled = false;

        //GUI variables.
        private Texture2D logo;
        private GUISkin skin;
        private const string root = "Assets/3DSauce/Breakout Pro for Playmaker/Tools/Editor/";
        private const string logoPath = root + "Skin/Images/logo.png";
        private const string logoIcon = root + "Skin/Images/favicon.png";
        private const string skinPath = root + "Skin/skin.guiskin";
        private const string infoOnIcon = root + "Skin/Images/infoOn.png";
        private const string infoOffIcon = root + "Skin/Images/infoOff.png";
        private const string infoOnIconFree = root + "Skin/Images/infoOn_Free.png";
        private const string infoOffIconFree = root + "Skin/Images/infoOff_Free.png";
        private const string dividerImage = root + "Skin/Images/boxDivider.png";
        private const string dividerImageFree = root + "Skin/Images/boxDivider_Free.png";
        private const string buttonPress = root + "Skin/Images/buttonPressed.png";
        private const string buttonRelease = root + "Skin/Images/buttonReleased.png";
        private const string buttonPressFree = root + "Skin/Images/buttonPressed_Free.png";
        private const string buttonReleaseFree = root + "Skin/Images/buttonReleased_Free.png";
        public Vector2 scrollPosition;
        public Font fontHeader;

        //Object replacement variables.
        public GameObject myObject;

        #endregion

        void Awake()
        {
            logo = AssetDatabase.LoadAssetAtPath(logoPath, typeof(Texture2D)) as Texture2D;
            skin = AssetDatabase.LoadAssetAtPath(skinPath, typeof(GUISkin)) as GUISkin;

            //Load EditorPrefs.
            snapEnabled = EditorPrefs.GetBool("3DSTools_snapEnabled", false);
            gridSizeHorizontal = EditorPrefs.GetFloat("3DSTools_gridSizeX", 1.0f);
            gridSizeVertical = EditorPrefs.GetFloat("3DSTools_gridSizeY", 0.4f);

            //Gets the header font.
            foreach (string guid in AssetDatabase.FindAssets("Ubuntu-B"))
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                fontHeader = AssetDatabase.LoadAssetAtPath(assetPath, typeof(Font)) as Font;
            }
        }
        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/3D Sauce/3D Sauce Helper")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            BreakoutHelper window = (BreakoutHelper)EditorWindow.GetWindow(typeof(BreakoutHelper));

            //New Unity 5 code for icon beside title.
            Texture icon = AssetDatabase.LoadAssetAtPath(logoIcon, typeof(Texture2D)) as Texture2D;
            GUIContent titleContent = new GUIContent("3D Sauce", icon);
            window.titleContent = titleContent;
            //Old Unity4//window.title = "3D Sauce";
            window.minSize = new Vector2(220, 250);
            window.Show();
        }

        void OnGUI()
        {
            //Begin area and then begin the scrollview.
            GUILayout.BeginArea(new Rect(0, 0, position.width, position.height));
            scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(position.width), GUILayout.Height(position.height));

            if (GUILayout.Button(logo))
            {
                //This will occur when the 3D Sauce benner is pressed...
                GUI.FocusControl("Clear");
                Application.OpenURL("http://3dsauce.com/");
            }

            if (skin != null) GUI.skin = skin;

            //Define style variable for header labels.
            GUIStyle labelStyleHeader = new GUIStyle();
            //Define style variable for tiny labels.
            GUIStyle labelStyle = new GUIStyle();
            //Define style variable for buttons.
            GUIStyle buttonStyle= new GUIStyle();
            //Define style variable for section dividers.
            GUIStyle dividerStyle = new GUIStyle();
            //Define style variable for info toggles
            GUIStyle infoStyle = new GUIStyle();

            if (Application.HasProLicense())
            {
                //Define style for header labels in Unity Pro.
                labelStyleHeader.fontSize = 14;
                labelStyleHeader.normal.textColor = new Color(195 / 255F, 173 / 255F, 100 / 255F, 1);
                //labelStyleHeader.contentOffset = new Vector2(6, 8);
                //labelStyleHeader.fontStyle = FontStyle.Bold;
                labelStyleHeader.font = fontHeader;
                labelStyleHeader.margin.left = 4;
                labelStyleHeader.margin.right = 4;
                labelStyleHeader.margin.top = 6;
                labelStyleHeader.margin.bottom = 6;
                labelStyleHeader.padding.left = 2;
                labelStyleHeader.padding.right = 2;
                labelStyleHeader.padding.top = 1;
                labelStyleHeader.padding.bottom = 2;
                labelStyleHeader.alignment = TextAnchor.MiddleLeft;

                //Define style for tiny labels in Unity Pro.
                labelStyle.fontSize = 10;
                labelStyle.normal.textColor = new Color(195 / 255F, 195 / 255F, 195 / 255F, 1);
                labelStyle.contentOffset = new Vector2(6, 10);

                //Define style for buttons in Unity Pro.
                buttonStyle.normal.background = AssetDatabase.LoadAssetAtPath(buttonRelease, typeof(Texture2D)) as Texture2D;
                buttonStyle.normal.textColor = new Color(195 / 255F, 195 / 255F, 195 / 255F, 1);
                buttonStyle.active.background = AssetDatabase.LoadAssetAtPath(buttonPress, typeof(Texture2D)) as Texture2D;
                buttonStyle.active.textColor = new Color(184 / 255F, 221 / 255F, 172 / 255F, 1);
                buttonStyle.margin.left = 4;
                buttonStyle.margin.right = 4;
                buttonStyle.margin.top = 8;
                buttonStyle.font = fontHeader;
                buttonStyle.fontSize = 14;
                buttonStyle.alignment = TextAnchor.MiddleCenter;

                //Define style for dividers in Unity Pro.
                dividerStyle.normal.background = AssetDatabase.LoadAssetAtPath(dividerImage, typeof(Texture2D)) as Texture2D;

                //Define style for info toggles in Unity Pro.
                infoStyle.onNormal.background = AssetDatabase.LoadAssetAtPath(infoOnIcon, typeof(Texture2D)) as Texture2D;
                infoStyle.normal.background = AssetDatabase.LoadAssetAtPath(infoOffIcon, typeof(Texture2D)) as Texture2D;
            }
            else
            {
                //Define style for header labels in Unity Free.
                labelStyleHeader.fontSize = 14;
                labelStyleHeader.normal.textColor = new Color(69 / 255F, 69 / 255F, 69 / 255F, 1);
                //labelStyleHeader.contentOffset = new Vector2(6, 8);
                //labelStyleHeader.fontStyle = FontStyle.Bold;
                labelStyleHeader.font = fontHeader;
                labelStyleHeader.margin.left = 4;
                labelStyleHeader.margin.right = 4;
                labelStyleHeader.margin.top = 6;
                labelStyleHeader.margin.bottom = 6;
                labelStyleHeader.padding.left = 2;
                labelStyleHeader.padding.right = 2;
                labelStyleHeader.padding.top = 1;
                labelStyleHeader.padding.bottom = 2;
                labelStyleHeader.alignment = TextAnchor.MiddleLeft;

                //Define style for tiny labels in Unity Free.
                labelStyle.fontSize = 10;
                labelStyle.normal.textColor = new Color(28 / 255F, 28 / 255F, 28 / 255F, 1);
                labelStyle.contentOffset = new Vector2(6, 10);

                //Define style for buttons in Unity Free.
                buttonStyle.normal.background = AssetDatabase.LoadAssetAtPath(buttonReleaseFree, typeof(Texture2D)) as Texture2D;
                buttonStyle.normal.textColor = new Color(220 / 255F, 220 / 255F, 220 / 255F, 1);
                buttonStyle.active.background = AssetDatabase.LoadAssetAtPath(buttonPressFree, typeof(Texture2D)) as Texture2D;
                buttonStyle.active.textColor = new Color(184 / 255F, 221 / 255F, 172 / 255F, 1);
                buttonStyle.margin.left = 4;
                buttonStyle.margin.right = 4;
                buttonStyle.margin.top = 8;
                buttonStyle.font = fontHeader;
                buttonStyle.fontSize = 14;
                buttonStyle.alignment = TextAnchor.MiddleCenter;

                //Define style for dividers in Unity Free.
                dividerStyle.normal.background = AssetDatabase.LoadAssetAtPath(dividerImageFree, typeof(Texture2D)) as Texture2D;

                //Define style for info toggles in Unity Free.
                infoStyle.onNormal.background = AssetDatabase.LoadAssetAtPath(infoOnIconFree, typeof(Texture2D)) as Texture2D;
                infoStyle.normal.background = AssetDatabase.LoadAssetAtPath(infoOffIconFree, typeof(Texture2D)) as Texture2D;
            }

            //Section spacer.
            GUILayout.Box("", dividerStyle, GUILayout.ExpandWidth(true), GUILayout.Height(4));
            GUILayout.Space(5);

            #region SnapToggle

            //Section header and info tooltip.
            GUILayout.BeginHorizontal(GUILayout.Width(195));
            GUILayout.Label("Grid Snapping", labelStyleHeader);
            infoSnappingEnabled = GUILayout.Toggle(infoSnappingEnabled, GUIContent.none, infoStyle, GUILayout.MaxWidth(24), GUILayout.MinHeight(24));
            GUILayout.EndHorizontal();
            if (infoSnappingEnabled) EditorGUILayout.HelpBox("Snapping will assist with quick and accurate object placement. Toggle it on to enable.\n\nGrid Size - Distance between each snap interval.", MessageType.Info);

            GUILayout.Space(5);

            //Begin area for snap toggle.
            GUILayout.BeginHorizontal(GUILayout.Width(195));

            GUILayout.Label("Snap On / Off", labelStyle);
            bool snapBefore = snapEnabled;
            snapEnabled = GUILayout.Toggle(snapEnabled, GUIContent.none, GUILayout.MinWidth(55), GUILayout.MinHeight(30));
            if (snapEnabled != snapBefore)
            {
                GUI.FocusControl("Clear");
                EditorPrefs.SetBool("3DSTools_snapEnabled", snapEnabled);
            }
            GUILayout.EndHorizontal();
            #endregion

            if (snapEnabled)
            {
                GUILayout.Space(5);

                //Set vertical offset for next two labels.
                labelStyle.contentOffset = new Vector2(6, 4);

                #region GridSize
                GUILayout.BeginHorizontal(GUILayout.Width(195));

                GUILayout.Label("Grid Size Horizontal", labelStyle);

                float tempGridSizeX = EditorGUILayout.FloatField(gridSizeHorizontal, GUILayout.MaxWidth(55));

                if (tempGridSizeX != 0 && !float.IsInfinity(tempGridSizeX) && tempGridSizeX >= 0.01f && tempGridSizeX <= 1f)
                {
                    gridSizeHorizontal = tempGridSizeX;
                    EditorPrefs.SetFloat("3DSTools_gridSizeX", gridSizeHorizontal);
                }

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal(GUILayout.Width(195));

                GUILayout.Label("Grid Size Vertical", labelStyle);

                float tempGridSizeY = EditorGUILayout.FloatField(gridSizeVertical, GUILayout.MaxWidth(55));

                if (tempGridSizeY != 0 && !float.IsInfinity(tempGridSizeY) && tempGridSizeY >= 0.01f && tempGridSizeY <= 1f)
                {
                    gridSizeVertical = tempGridSizeY;
                    EditorPrefs.SetFloat("3DSTools_gridSizeY", gridSizeVertical);
                }

                GUILayout.EndHorizontal();

                if (GUILayout.Button("Reset Values", buttonStyle, GUILayout.MaxWidth(195), GUILayout.MinHeight(30)))
                {
                    GUI.FocusControl("Clear");
                    gridSizeHorizontal = 1.0f;
                    gridSizeVertical = 0.4f;
                }
                #endregion
            }

            //Adjust style for the remaining labels.
            labelStyle.contentOffset = new Vector2(6, 0);

            //Section spacer.
            GUILayout.Space(14);
            GUILayout.Box("", dividerStyle, GUILayout.ExpandWidth(true), GUILayout.Height(4));
            GUILayout.Space(5);

            #region Replace Tools

            //Section header and info tooltip.
            GUILayout.BeginHorizontal(GUILayout.Width(195));
            GUILayout.Label("Replace Bricks", labelStyleHeader);
            infoPrefabsEnabled = GUILayout.Toggle(infoPrefabsEnabled, GUIContent.none, infoStyle, GUILayout.MaxWidth(24), GUILayout.MinHeight(24));
            GUILayout.EndHorizontal();
            if (infoPrefabsEnabled) EditorGUILayout.HelpBox("Replace will assist in replacing multiple bricks simultaneously. There are two ways in which this tool can be used.\n\n(Method A)\n1. Choose a prefab from your project\n2. Select all of the scene objects you wish to have replaced.\n3. Press the Replace Selected button.\n\n(Method B)\n1. Select all of the scene objects you wish to have replaced.\n2. Hold CTRL and select a prefab from your project.\n3. Press the keyboard hotkey combo Ctrl+Shift+W.", MessageType.Info);

            GUILayout.Space(5);

            //Replace selected objects interface.
            GUILayout.Label("Choose Prefab:", labelStyle);
            GUI.SetNextControlName("prefabField");
            myObject = EditorGUILayout.ObjectField(myObject, typeof(GameObject), true, GUILayout.MaxWidth(195)) as GameObject;
            if (GUILayout.Button("Replace Selected", buttonStyle, GUILayout.MaxWidth(195), GUILayout.MinHeight(30)))
            {
                GUI.FocusControl("Clear");
                Sauce3D.SelectionReplace.myObject = myObject;
                Sauce3D.SelectionReplace.ReplaceSelected();
            }
            #endregion

            //End scrollview and then end area.
            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }

        void Update()
        {
            if (!snapEnabled) return;
            if (Selection.transforms.Length <= 0) return;
            Transform first = Selection.transforms[0];

            if (posBefore != first.position)
            {
                SnapToGrid(Selection.transforms);
                posBefore = first.position;
            }
        }

        #region SnapToGrid
        private void SnapToGrid(Transform[] transforms)
        {
            foreach (Transform t in transforms)
            {
                if (t.gameObject.tag == "Brick" || t.gameObject.tag == "BrickCage")
                {
                    //Vector3 pos = t.position;

                    Undo.RecordObject(t, "Snap to Grid");
                    float newX = RoundToS(t.localPosition.x, gridSizeHorizontal);
                    float newY = RoundToS(t.localPosition.y, gridSizeVertical);
                    //float newZ = RoundToS(t.position.z, gridSize);
                    float newZ = 0;

                    t.localPosition = new Vector3(newX, newY, newZ);
                    EditorUtility.SetDirty(t);
                }
            }
        }

        private float RoundToS(float f, float s)
        {
            return (Mathf.RoundToInt(f / s) * s);
        }
        #endregion
    }
}