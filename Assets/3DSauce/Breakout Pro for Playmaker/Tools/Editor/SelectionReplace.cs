﻿ using UnityEngine;
 using UnityEditor;

namespace Sauce3D
{
    public class SelectionReplace : MonoBehaviour
    {
        public static GameObject myObject;
        private static int prefabCount;

        [MenuItem("GameObject/3D Sauce/Replace Selected With Prefab %#w")]
        #region Replace Objects Using Hotkeys
        static void QuickReplace()
        {
            myObject = null;
            prefabCount = 0;
            GameObject[] gameObjects = Selection.gameObjects;
            foreach (GameObject go in gameObjects)
            {
                if (PrefabUtility.GetPrefabType(go) == PrefabType.Prefab)
                {
                    prefabCount++;
                    myObject = go;
                }
            }

            if (prefabCount > 1)
            {
                Debug.Log("too many prefabs");
                return;
            }

            if (myObject != null)
            {
                if (PrefabUtility.GetPrefabType(myObject) == PrefabType.Prefab)
                {
                    if (myObject != null)
                    {
                        foreach (Transform t in Selection.transforms)
                        {
                            if (PrefabUtility.GetPrefabType(t) != PrefabType.Prefab)
                            {
                                GameObject o = null;
                                o = PrefabUtility.GetPrefabParent(myObject) as GameObject;

                                if (PrefabUtility.GetPrefabType(myObject).ToString() == "PrefabInstance")
                                {
                                    o = (GameObject)PrefabUtility.InstantiatePrefab(o);
                                    PrefabUtility.SetPropertyModifications(o, PrefabUtility.GetPropertyModifications(myObject));
                                }

                                else if (PrefabUtility.GetPrefabType(myObject).ToString() == "Prefab")
                                {
                                    o = (GameObject)PrefabUtility.InstantiatePrefab(myObject);
                                }

                                else
                                {
                                    o = Instantiate(myObject) as GameObject;
                                }

                                Undo.RegisterCreatedObjectUndo(o, "created prefab");

                                Transform newT = o.transform;

                                if (t != null)
                                {
                                    newT.position = t.position;
                                    newT.rotation = t.rotation;
                                    newT.localScale = t.localScale;
                                    newT.parent = t.parent;
                                }
                            }
                        }

                        foreach (GameObject go in Selection.gameObjects)
                        {
                            if (PrefabUtility.GetPrefabType(go) != PrefabType.Prefab)
                            {
                                Undo.DestroyObjectImmediate(go);
                            }
                        }
                    }
                }
            }
            //If no prefab is chosen.
            else
            {
                Debug.Log("You must select a replacement prefab from your project.");
                return;
            }
        }
        #endregion

        #region Replace Objects Using 3D Sauce Helper
        public static void ReplaceSelected()
        {
            if (myObject != null)
            {
                foreach (Transform t in Selection.transforms)
                {
                    if (PrefabUtility.GetPrefabType(t) != PrefabType.Prefab)
                    {
                        GameObject o = null;
                        o = PrefabUtility.GetPrefabParent(myObject) as GameObject;

                        if (PrefabUtility.GetPrefabType(myObject).ToString() == "PrefabInstance")
                        {
                            o = (GameObject)PrefabUtility.InstantiatePrefab(o);
                            PrefabUtility.SetPropertyModifications(o, PrefabUtility.GetPropertyModifications(myObject));
                        }

                        else if (PrefabUtility.GetPrefabType(myObject).ToString() == "Prefab")
                        {
                            o = (GameObject)PrefabUtility.InstantiatePrefab(myObject);
                        }

                        else
                        {
                            o = Instantiate(myObject) as GameObject;
                        }

                        Undo.RegisterCreatedObjectUndo(o, "created prefab");

                        Transform newT = o.transform;

                        if (t != null)
                        {
                            newT.position = t.position;
                            newT.rotation = t.rotation;
                            newT.localScale = t.localScale;
                            newT.parent = t.parent;
                        }
                    }
                }

                foreach (GameObject go in Selection.gameObjects)
                {
                    if (PrefabUtility.GetPrefabType(go) != PrefabType.Prefab)
                    {
                        Undo.DestroyObjectImmediate(go);
                    }
                }
            }
            //If no prefab is chosen.
            if (myObject == null)
            {
                EditorGUI.FocusTextInControl("prefabField");
                Debug.Log("Must define a prefab before replacing objects.");
            }
        }
        #endregion
    }
}