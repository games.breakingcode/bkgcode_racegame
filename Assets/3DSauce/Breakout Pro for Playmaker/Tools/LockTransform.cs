﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[HideInInspector]
public class LockTransform : MonoBehaviour
{
    private bool prevLocked;
    private Vector3 lockedPosition;
    public bool DestoryNextOpportunity { get; set; }

    // Use this for initialization
    void Start()
    {
        //this.hideFlags = HideFlags.NotEditable;
        lockedPosition = transform.localPosition;

        if (Application.isPlaying)
            Destroy(this);
    }

    void OnEnable()
    {
        lockedPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = lockedPosition;
        if (DestoryNextOpportunity)
            DestroyImmediate(this); // Only called from editor
    }
}