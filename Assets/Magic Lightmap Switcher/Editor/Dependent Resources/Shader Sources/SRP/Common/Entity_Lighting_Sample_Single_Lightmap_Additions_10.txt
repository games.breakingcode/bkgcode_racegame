    //<MLS_ENTITY_LIGHTING_SAMLE_SINGLE_LIGHTMAP_ADDITIONS>
    // Original code
    /*if (encodedLightmap)
    {
        real4 encodedIlluminance = SAMPLE_TEXTURE2D_LIGHTMAP(lightmapTex, lightmapSampler, LIGHTMAP_EXTRA_ARGS_USE).rgba;
        illuminance = DecodeLightmap(encodedIlluminance, decodeInstructions);
    }
    else
    {
        illuminance = SAMPLE_TEXTURE2D_LIGHTMAP(lightmapTex, lightmapSampler, LIGHTMAP_EXTRA_ARGS_USE).rgb;
    }*/

    // Magic Lightmap Switcher
    if (encodedLightmap)
    {
        real4 encodedIlluminance = lerp(
            SAMPLE_TEXTURE2D_LIGHTMAP(lightmapTex, lightmapSampler, LIGHTMAP_EXTRA_ARGS_USE).rgba,
            BlendTwoTextures(0, LIGHTMAP_EXTRA_ARGS_USE, lightmapSampler).rgba,
            _MLS_ENABLE_LIGHTMAPS_BLENDING);

        illuminance = DecodeLightmap(encodedIlluminance, decodeInstructions);        
    }
    else
    {
        illuminance = lerp(
            SAMPLE_TEXTURE2D_LIGHTMAP(lightmapTex, lightmapSampler, LIGHTMAP_EXTRA_ARGS_USE).rgb,
            BlendTwoTextures(0, LIGHTMAP_EXTRA_ARGS_USE, lightmapSampler).rgb,
            _MLS_ENABLE_LIGHTMAPS_BLENDING);             
    }
    //</MLS_ENTITY_LIGHTING_SAMLE_SINGLE_LIGHTMAP_ADDITIONS>