//<MLS_COMMON_INCLUDES>
#define MLS_LIGHTMAPS_BLENDING_ENABLED
// Lightmaps Processing
float _MLS_Lightmaps_Blend_Factor;

TEXTURE2D(_MLS_Lightmap_Color_Blend_From);
TEXTURE2D(_MLS_Lightmap_Color_Blend_To); 
TEXTURE2D(_MLS_Lightmap_Dir_Blend_From);
TEXTURE2D(_MLS_Lightmap_Dir_Blend_To);

// Reflections Prcessing
float _MLS_Reflections_Blend_Factor;
int _MLS_ReflectionsFlag;

TEXTURECUBE(_MLS_Reflection_Blend_From_0);
TEXTURECUBE(_MLS_Reflection_Blend_To_0);
TEXTURECUBE(_MLS_Reflection_Blend_From_1);
TEXTURECUBE(_MLS_Reflection_Blend_To_1);
TEXTURECUBE(_MLS_SkyReflection_Blend_From);
TEXTURECUBE(_MLS_SkyReflection_Blend_To);

// General 
int _MLS_ENABLE_LIGHTMAPS_BLENDING;
int _MLS_ENABLE_REFLECTIONS_BLENDING;
int _MLS_No_Decode;

float4 BlendTwoTextures(int lightmapType, float2 uv, SamplerState samplerState)
{
    half4 textureFrom;
    half4 textureTo;

    switch (lightmapType)
    {
    case 0:
        textureFrom = SAMPLE_TEXTURE2D(_MLS_Lightmap_Color_Blend_From, samplerState, uv.xy);
        textureTo = SAMPLE_TEXTURE2D(_MLS_Lightmap_Color_Blend_To, samplerState, uv.xy);
        break;
    case 1:
        textureFrom = SAMPLE_TEXTURE2D(_MLS_Lightmap_Dir_Blend_From, samplerState, uv.xy);
        textureTo = SAMPLE_TEXTURE2D(_MLS_Lightmap_Dir_Blend_To, samplerState, uv.xy);
        break;
    }

    return lerp(textureFrom, textureTo, _MLS_Lightmaps_Blend_Factor);
}

float4 BlendTwoCubeTextures(int probeIndex, float3 reflection, half mip, SamplerState samplerState)
{
    float4 textureFrom;
    float4 textureTo;

    switch (probeIndex)
    {
    case 0:
        textureFrom = SAMPLE_TEXTURECUBE_LOD(_MLS_Reflection_Blend_From_0, samplerState, reflection, mip);
        textureTo = SAMPLE_TEXTURECUBE_LOD(_MLS_Reflection_Blend_To_0, samplerState, reflection, mip);
        break;
    case 1:
        textureFrom = SAMPLE_TEXTURECUBE_LOD(_MLS_Reflection_Blend_From_1, samplerState, reflection, mip);
        textureTo = SAMPLE_TEXTURECUBE_LOD(_MLS_Reflection_Blend_To_1, samplerState, reflection, mip);
        break;
    case 2:
        textureFrom = SAMPLE_TEXTURECUBE_LOD(_MLS_SkyReflection_Blend_From, samplerState, reflection, mip);
        textureTo = SAMPLE_TEXTURECUBE_LOD(_MLS_SkyReflection_Blend_To, samplerState, reflection, mip);
        break;
    }

    return lerp(textureFrom, textureTo, _MLS_Reflections_Blend_Factor);
}
//</MLS_COMMON_INCLUDES>